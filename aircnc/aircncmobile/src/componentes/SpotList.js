import React, { useEffect, useState } from 'react';
import { withNavigation } from 'react-navigation';
import {View, StyleSheet, Text, FlatList, Image} from 'react-native';

import api from '../services/api';
import { TouchableOpacity } from 'react-native-gesture-handler';

 function SpotList({tech, navigation}){
    const [spots, setSpots] = useState([]);
    useEffect(()=> {
        async function loadSpots(){
            const respose = await api.get('/spots',{
                params: {tech}
            })

            setSpots(respose.data);
        }

        loadSpots();
    },[]);
    function handleNavigation(id){
        navigation.navigate('Book',{id});
    }
    return <View style={styles.container}>
                    <Text style={styles.titl}e> Empresas que usam <Text style={styles.bold}>{tech}</Text></Text>
                    <FlatList 
                          style={styles.list}
                          data={spots}
                          keyExtractor={spot=>spot._id}
                          horizontal
                          showsHorizontalScrollIndicator={false}
                          renderItem={({ item })=>( 
                              <View style={styles.listItem}>
                                        <Image style={styles.thumbnail} source={{uri: item.thumbnail_url.replace('localhost','192.168.1.2')}} />
                                        <Text style={styles.company}>{item.company}</Text>
                                        <Text style={styles.price}>{item.price? `R$${item.price}/Dia`:'GRATUITO'}</Text>
                                        <TouchableOpacity  onPress={()=>handleNavigation(item._id)} style={styles.button} >
                                            <Text style={styles.textButton}> Solicitar Reserva</Text>
                                        </TouchableOpacity>
                              </View>
                          )}
                    />
        </View>
}

const styles = StyleSheet.create({
    container: {
            marginTop: 20,
    },
    title:{
        fontSize:20,
        color: "#444",
        paddingHorizontal: 20,
        marginBottom: 15
    },
    bold:{
        fontWeight: "bold"
    },
    list:{
        paddingHorizontal:20,
    },
    listItem:{
        marginRight:15,
    },
    thumbnail:{
        width:200,
        height:120,
        resizeMode: "cover",
        borderRadius:2,
    },
    company:{
        fontSize:24,
        fontWeight:'bold',
        color:"#333",
        marginTop: 10,
    },
    price:{
        fontSize:15,
        color:"#999",
        marginTop:5
    },
    button:{
        height:32,
        backgroundColor:"#f05a5b",
        justifyContent:'center',
        alignItems:'center',
        borderRadius:2,
        marginTop:15,
    },
    textButton:{
        color:"#fff",
        fontWeight:'bold',
        fontSize:15,
    }
})

export default withNavigation(SpotList);